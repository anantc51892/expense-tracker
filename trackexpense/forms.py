from django import forms
from . import models

class NewExpense(forms.ModelForm):
	class Meta:
		model = models.Expense
		fields = ['title','amount','desc', 'category']

class NewCategory(forms.ModelForm):
	class Meta:
		model = models.Category
		fields = ['title'] 