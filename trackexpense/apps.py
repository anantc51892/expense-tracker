from django.apps import AppConfig


class TrackexpenseConfig(AppConfig):
    name = 'trackexpense'
