from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth.decorators import login_required
from datetime import datetime
# Create your views here.
from .models import Expense, Category
from . import forms


@login_required(login_url = "/accounts/login")
def index(request):
	userr = request.user
	expenses_list = Expense.objects.filter(author = userr).filter(pub_date__lte= datetime.now()).order_by('-pub_date')
	return render(request,'main/index.html',{'expenses_list': expenses_list})

@login_required(login_url = "/accounts/login")
def CategoryView(request):
	category_list = Category.objects.filter(author = request.user).order_by('title')
	return render(request, 'main/Category.html',{'category_list': category_list})
	
@login_required(login_url = "/accounts/login")
def NewExpense(request):
	if request.method == 'POST':
		form = forms.NewExpense(request.POST)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.author = request.user
			instance.save()
			return redirect('expenses:dashboard')
	else:
		form = forms.NewExpense()
	return render(request,'main/NewExpense.html', {'form':form})

@login_required(login_url = "/accounts/login")
def NewCategory(request):
	if request.method == 'POST':
		form = forms.NewCategory(request.POST)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.author = request.user
			instance.save()
			return redirect('expenses:dashboard')
	else:
		form = forms.NewCategory()
	return render(request,'main/NewCategory.html', {'form':form})

@login_required(login_url = "/accounts/login")
def LabelView(request, title):
	userr = request.user
	expenses_list = Expense.objects.filter(author=userr, category__title = title, pub_date__lte= datetime.now(), ).order_by('-pub_date')
	return render(request,'main/index.html',{'expenses_list': expenses_list})
	