from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User

# Create your models here.
class Category(models.Model):
	title = models.CharField(max_length = 50)
	author = models.ForeignKey(User,on_delete = models.CASCADE)
	def __str__(self):
		return self.title

class Expense(models.Model):
	title = models.CharField(max_length = 50)
	pub_date = models.DateTimeField(auto_now_add= True)
	author = models.ForeignKey(User,default=None,on_delete=models.CASCADE)
	amount = models.IntegerField()
	desc = models.CharField(max_length = 200, verbose_name='Description')
	category = models.ForeignKey(Category, default=None, on_delete = models.CASCADE, null=True,blank=True)
	def __str__(self):
		return self.title
	
class Bill(models.Model):
	title = models.CharField(max_length = 50)
	due_date = models.DateTimeField(auto_now_add= True)
	author = models.ForeignKey(User,default=None,on_delete=models.CASCADE)
	amount = models.IntegerField()
	desc = models.CharField(max_length = 200, verbose_name='Description')
	category = models.ForeignKey(Category, default=None, on_delete = models.CASCADE, null=True,blank=True)
	def __str__(self):
		return self.title