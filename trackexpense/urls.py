from django.urls import path


from . import views

app_name = 'expenses'

urlpatterns = [
	path('', views.index, name = 'dashboard'),
	path('addexpense/', views.NewExpense, name = 'create'),
	path('labels/',views.CategoryView, name = 'labels'),
	path('labels/add', views.NewCategory, name = 'addlabel'),
	path('labels/<str:title>', views.LabelView, name = 'labelview'),
]