from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import generic
from django.template import loader
from django.contrib.auth.decorators import login_required

from . import forms


# Create your views here.
def signup_view(request):
	if request.user.is_authenticated:
		return redirect('expenses:dashboard')
	else:
		if request.method == 'POST':
			form = forms.SignUpForm(request.POST)
			if form.is_valid():
				user = form.save()
				login(request, user)
				return redirect('expenses:dashboard')
		else:
			form = forms.SignUpForm()
		return render(request,'accounts/signup.html',{'form': form})


def login_view(request):
	if request.user.is_authenticated:
		return redirect('expenses:dashboard')
	else:
		if request.method == 'POST':
			form = AuthenticationForm(data = request.POST)
			if form.is_valid():
				user = form.get_user()
				login(request, user)
				if 'next' in request.POST:
					return redirect(request.POST.get('next'))
				else:
					return redirect('expenses:dashboard')
		else:
			form = AuthenticationForm()
		return render(request, 'accounts/login.html',{'form' : form})

def logout_view(request):
	if request.method == 'POST':
		logout(request)
		return redirect('accounts:login')

@login_required(login_url = "/accounts/login")
def profile(request):
	userr = request.user